using Microsoft.EntityFrameworkCore;

namespace AppointmentApi.Models
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {
        }

        public DbSet<AppointmentModel> Appointments { get; set; }
    }
}