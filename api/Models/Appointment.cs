namespace AppointmentApi.Models
{
    public class AppointmentModel
    {
        public long Id { get; set; }

        public string AppointmentTime { get; set; }

        public string Doctor { get; set; }

        public string Patient { get; set; }

        public string PatientEmailAddress { get; set; }
    }
}