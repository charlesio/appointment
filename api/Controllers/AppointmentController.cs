using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AppointmentApi.Models;


namespace AppointmentApi.Controllers
{
    [Route("api/appointment")]
    [ApiController]
    public class AppointmentController : ControllerBase
    {
        private readonly Context _context;

        public AppointmentController(Context context)
        {
            _context = context;
        }

        ///<summary>Creates a new appointment.</summary>
        ///<param><see cref="Appointment"></param>
        ///<returns>A new appointment.</returns>
        [HttpPost]
        public async Task<ActionResult<AppointmentModel>> CreateAppointment(AppointmentModel appointment)
        {
            _context.Appointments.Add(appointment);
            await _context.SaveChangesAsync();
            // TODO Send email - Does not work at the moment. 
            // SendEmail(appointment);
            return CreatedAtAction(nameof(GetAppointment), new { id = appointment.Id}, appointment);
        }

        ///<summary>Delets a single appointment.</summary>
        ///<param name="id">Id <see cref="long"></param>
        [HttpDelete]
        public async Task<IActionResult> DeleteAppointment(long id) {
            var appointment = await _context.Appointments.FindAsync(id);

            if (appointment == null) return NotFound();
            
            _context.Appointments.Remove(appointment);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        ///<summary>Gets a single appointment</summary>
        ///<param name="id">Id <see cref="long"></param>
        ///<returns>A single <see cref="Appointment"></param>
        [HttpGet("{id}")]
        public async Task<ActionResult<AppointmentModel>> GetAppointment(long id)
        {
            var appointment = await _context.Appointments.FindAsync(id);

            if (appointment == null) return NotFound();
            
            return appointment;
        }

        ///<summary>Gets all appointments.</summary>
        ///<returns>A <see cref="List"> of appointments.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AppointmentModel>>> GetAppointments() => await _context.Appointments.ToListAsync();

        [HttpPut("{id}")]
        public async Task<IActionResult> ModifyAppointment(long id, AppointmentModel appointment)
        {
            if (id != appointment.Id) return BadRequest();

            _context.Entry(appointment).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();

        }

        ///<summary>Sends an email to the patient.</summary>
        ///<param name="appointment">An appointment.</param>
        private void SendEmail(AppointmentModel appointment) {
            var client = new SmtpClient() 
            {
                Port = 587,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = "smtp.gmail.com",
                EnableSsl = true,
                Credentials = new NetworkCredential("", "")
            };

            client.UseDefaultCredentials = false;
            
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("noreply@mailinator.com");
            mailMessage.To.Add(appointment.PatientEmailAddress);
            mailMessage.Body = $"Appointment with {appointment.Doctor} on {appointment.AppointmentTime}";
            mailMessage.Subject = "Doctor Appointment";
            client.Send(mailMessage);
        }
    }
}