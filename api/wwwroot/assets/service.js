app.service("appointmentService", function ($http) {
    this.create = function (appointment) {
        var response = $http({
            method: "post",
            url: "/api/appointment",
            data: JSON.stringify(appointment),
            dataType: "json"
        });
        return response;
    }
});