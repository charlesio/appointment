app.controller("appointmentCtrl", function ($scope, appointmentService) {

    $scope.Save = function() {
        Create();
    }

    function Create() {
        var appointment = { 
            AppointmentTime : $scope.AppointmentTime,
            Doctor : $scope.Doctor,
            Patient : $scope.PatientName,
            PatientEmailAddress : $scope.PatientEmail
        };
        
        appointmentService.create(appointment);
    }
});